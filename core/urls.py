from django.urls import path

from .views import hello_world_view


app_name = "core"
urlpatterns = [
    path("", hello_world_view, name="hello_world"),
]
